﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WoWAddonUpdater
{
    internal class AddonData
    {
        internal class Author
        {
            public string Name { get; set; }
            public string Url { get; set; }
            public uint ProjectId { get; set; }
            public uint Id { get; set; }
            public uint? ProjectTitleId { get; set; }
            public string ProjectTitle { get; set; }
            public uint UserId { get; set; }
            public uint? TwitchId { get; set; }
        }

        internal class Attachment
        {
            public uint Id { get; set; }
            public uint ProjectId { get; set; }
            public string Description { get; set; }
            public bool IsDefault { get; set; }
            public string ThumbnailUrl { get; set; }
            public string Title { get; set; }
            public string Url { get; set; }
            public uint Status { get; set; }
        }

        internal class LatestFile
        {
            internal class Dependency
            {
                public uint Id { get; set; }
                public uint AddonId { get; set; }
                public uint Type { get; set; }
                public uint FileId { get; set; }
            }

            internal class Module
            {
                public string Foldername { get; set; }
                public uint Fingerprint { get; set; }
                public uint Type { get; set; }
            }

            internal class GameVersionSortable
            {
                public string GameVersionPadded { get; set; }
                public string GameVersion { get; set; }
                public string GameVersionReleaseDate { get; set; }
                public string GameVersionName { get; set; }
            }

            public uint Id { get; set; }
            public string DisplayName { get; set; }
            public string FileName { get; set; }
            public string FileDate { get; set; }
            public uint FileLength { get; set; }
            public uint ReleaseType { get; set; }
            public uint FileStatus { get; set; }
            public string DownloadUrl { get; set; }
            public bool IsAlternate { get; set; }
            public uint AlternateFileId { get; set; }
            public List<Dependency> Dependencies { get; set; }
            public bool IsAvailable { get; set; }
            public List<Module> Modules { get; set; }
            public uint PackageFingerprint { get; set; }
            public List<string> GameVersion { get; set; }
            public List<GameVersionSortable> SortableGameVersion { get; set; }
            public string InstallMetaData { get; set; }
            public string Changelog { get; set; }
            public bool HasInstallScript { get; set; }
            public bool IsCompatibleWithClient { get; set; }
            public uint CategorySectionPackageType { get; set; }
            public uint RestrictProjectFileAccess { get; set; }
            public uint ProjectStatus { get; set; }
            public uint? RenderCacheId { get; set; }
            public uint? FileLegacyMappingId { get; set; }
            public uint ProjectId { get; set; }
            public uint? ParentProjectFileId { get; set; }
            public uint? ParentFileLegacyMappingId { get; set; }
            public uint? FileTypeId { get; set; }
            public string ExposeAsAlternative { get; set; }
            public uint PackageFingerprintId { get; set; }
            public string GameVersionDateReleased { get; set; }
            public uint GameVersionMappingId { get; set; }
            public uint GameVersionId { get; set; }
            public uint GameId { get; set; }
            public bool IsServerPack { get; set; }
            public uint? ServerPackFileId { get; set; }
            public string GameVersionFlavor { get; set; }
        }

        internal class Category
        {
            public uint CategoryId { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
            public string AvatarUrl { get; set; }
            public uint ParentId { get; set; }
            public uint RootId { get; set; }
            public uint ProjectId { get; set; }
            public uint AvatarId { get; set; }
            public uint GameId { get; set; }
        }

        internal class Section
        {
            public uint Id { get; set; }
            public uint GameId { get; set; }
            public string Name { get; set; }
            public uint PackageType { get; set; }
            public string Path { get; set; }
            public string InitialInclusionPattern { get; set; }
            public string ExtraIncludePattern { get; set; }
            public uint GameCategoryId { get; set; }
        }

        internal class GameVersionLatestFile
        {
            public string GameVersion { get; set; }
            public uint ProjectFileId { get; set; }
            public string ProjectFileName { get; set; }
            public uint FileType { get; set; }
            public string GameVersionFlavor { get; set; }
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public List<Author> Authors { get; set; }
        public List<Attachment> Attachments { get; set; }
        public string WebsiteUrl { get; set; }
        public uint GameId { get; set; }
        public string Summary { get; set; }
        public uint DefaultFileId { get; set; }
        public uint DonwloadCount { get; set; }
        public List<LatestFile> LatestFiles { get; set; }
        public List<Category> Categories { get; set; }
        public uint Status { get; set; }
        public uint PrimaryCategoryId { get; set; }
        public Section CategorySection { get; set; }
        public string Slug { get; set; }
        public List<GameVersionLatestFile> GameVersionLatestFiles { get; set; }
        public bool IsFeatured { get; set; }
        public float PopularityRank { get; set; }
        public uint GamePopularityRank { get; set; }
        public string PrimaryLanguage { get; set; }
        public string GameSlug { get; set; }
        public string GameName { get; set; }
        public string PortalName { get; set; }
        public string DataModified { get; set; }
        public string DataCreated { get; set; }
        public string DataReleased { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsExperimental { get; set; }
    }

    public class CurseForge
    {
        public CurseForge()
        {
            DataColumn IdColumn = new DataColumn();
            IdColumn.DataType = System.Type.GetType("System.UInt32");
            IdColumn.ColumnName = "ID";
            IdColumn.AutoIncrement = false;
            IdColumn.ReadOnly = true;

            DataColumn NameColumn = new DataColumn();
            NameColumn.DataType = System.Type.GetType("System.String");
            NameColumn.ColumnName = "Name";
            NameColumn.AutoIncrement = false;
            NameColumn.ReadOnly = true;

            Database.Columns.Add(IdColumn);
            Database.Columns.Add(NameColumn);

            DataColumn[] PrimaryKeyColumn = new DataColumn[1];
            PrimaryKeyColumn[0] = Database.Columns["ID"];
            Database.PrimaryKey = PrimaryKeyColumn;
        }

        public uint GetID(string AddonName)
        {
            try
            {
                DataRow[] Result = Database.Select("Name='" + AddonName + "'");
                return Result.Length == 0 ? 0 : Convert.ToUInt32(Result[0][0]);
            }
            catch (Exception Exception)
            {
                Logger.Log(Exception.ToString());
            }

            return 0;
        }

        public async Task UpdateDatabase()
        {
            using (HttpClient RestAPI = new HttpClient())
            {
                Database.Rows.Clear();

                RestAPI.BaseAddress = new Uri(BaseAddress);
                RestAPI.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                const uint PageSize = 10000;
                uint StartIndex = 0;

                while (true)
                {
                    string Request = "api/v2/addon/search?gameId=1&pageSize=" + PageSize + "&index=" + StartIndex;

                    HttpResponseMessage Response = RestAPI.GetAsync(Request).Result;

                    if (!Response.IsSuccessStatusCode)
                    {
                        break;
                    }

                    try
                    {
                        string JSON = await Response.Content.ReadAsStringAsync();
                        List<AddonData> Addons = JsonConvert.DeserializeObject<List<AddonData>>(JSON);

                        foreach (AddonData AddonData in Addons)
                        {
                            DataRow Row = Database.NewRow();
                            Row["ID"] = AddonData.Id;
                            Row["Name"] = GetFoldername(AddonData);
                            Database.Rows.Add(Row);
                        }
                    }
                    catch (Exception Exception)
                    {
                        Logger.Log(Exception.ToString());
                    }

                    StartIndex += PageSize;
                }
            }
        }

        private string GetFoldername(AddonData AddonData)
        {
            for (int Index = AddonData.LatestFiles.Count - 1; Index >= 0; --Index)
            {
                if (AddonData.LatestFiles[Index].ReleaseType == 1)
                {
                    foreach (AddonData.LatestFile.Module Module in AddonData.LatestFiles[Index].Modules)
                    {
                        if (Module.Type == 3)
                        {
                            return Module.Foldername;
                        }
                    }
                }
            }

            return string.Empty;
        }

        public async Task<string> GetDownloadLink(string Name)
        {
            using (HttpClient RestAPI = new HttpClient())
            {
                RestAPI.BaseAddress = new Uri(BaseAddress);
                RestAPI.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Request = "api/v2/addon/" + GetID(Name);

                HttpResponseMessage Response = RestAPI.GetAsync(Request).Result;

                if (Response.IsSuccessStatusCode)
                {
                    string JSON = await Response.Content.ReadAsStringAsync();

                    try
                    {
                        AddonData AddonData = JsonConvert.DeserializeObject<AddonData>(JSON);

                        for (int Index = AddonData.LatestFiles.Count - 1; Index >= 0; --Index)
                        {
                            if (AddonData.LatestFiles[Index].ReleaseType == 1)
                            {
                                foreach (AddonData.LatestFile.Module Module in AddonData.LatestFiles[Index].Modules)
                                {
                                    if (Module.Type == 3)
                                    {
                                        return AddonData.LatestFiles[Index].DownloadUrl;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception Exception)
                    {
                        Logger.Log(Exception.ToString());
                    }
                }
                else
                {
                    Logger.Log("Addon " + Name + " could not be found.");
                }
            }

            return string.Empty;
        }

        private DataTable Database = new DataTable("Addons");
        private readonly string BaseAddress = "https://addons-ecs.forgesvc.net";
    }
}
