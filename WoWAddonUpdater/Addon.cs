﻿namespace WoWAddonUpdater
{
    class Addon
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
        public uint CurseProjectID { get; set; }
    }
}