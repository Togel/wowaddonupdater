﻿using System.Collections.Generic;
using System.IO;

namespace WoWAddonUpdater
{
    class AddonFinder
    {
        public List<Addon> ScanRepository()
        {
            List<Addon> Addons = new List<Addon>();

            if (Directory.Exists(Properties.Settings.Default.AddonsDirectory))
            {
                foreach (string AddonDirectory in Directory.GetDirectories(Properties.Settings.Default.AddonsDirectory))
                {
                    string[] TocFiles = Directory.GetFiles(AddonDirectory, "*.toc");

                    if (TocFiles.Length > 0)
                    {
                        Addon Addon = new Addon();
                        Addon.Path = AddonDirectory;
                        Addon.Name = Path.GetFileName(AddonDirectory);

                        StreamReader Reader = new StreamReader(TocFiles[0]);
                        string Line;
                        while ((Line = Reader.ReadLine()) != null)
                        {
                            if (Line.StartsWith("## Version"))
                            {
                                Addon.Version = Line.Substring(Line.LastIndexOf(':') + 2);
                            }
                        }

                        Reader.Close();
                        Addons.Add(Addon);
                    }
                }
            }

            return Addons;
        }
    }
}
