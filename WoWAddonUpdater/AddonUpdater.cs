﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace WoWAddonUpdater
{
    class AddonUpdater
    {
        public void UpdateAddon(string DownloadURL)
        {
            using (WebClient WebClient = new WebClient())
            {
                try
                {
                    string TemporaryZipPath = Properties.Settings.Default.AddonsDirectory + @"\temp.zip";
                    WebClient.DownloadFile(DownloadURL, TemporaryZipPath);

                    using (ZipArchive Archive = ZipFile.OpenRead(TemporaryZipPath))
                    {
                        foreach (ZipArchiveEntry ZipEntry in Archive.Entries)
                        {
                            string ZipEntryPath = Path.GetFullPath(Path.Combine(Properties.Settings.Default.AddonsDirectory, ZipEntry.FullName));

                            if (ZipEntry.Name.Length == 0)
                            {
                                Directory.CreateDirectory(ZipEntryPath);
                            }
                            else
                            {
                                ZipEntry.ExtractToFile(ZipEntryPath, true);
                            }
                        }
                    }

                    File.Delete(TemporaryZipPath);
                }
                catch (Exception Exception)
                {
                    Logger.Log(Exception.ToString());
                }
            }
        }
    }
}
