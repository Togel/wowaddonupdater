﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace WoWAddonUpdater
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            UpdateApplication();

            Title += " v" + Version;
            AddonRepositoryBox.Text = Properties.Settings.Default.AddonsDirectory;

            if (Blacklist == null)
            {
                Blacklist = new List<uint>();
            }

            AddonList.SizeChanged += ListViewSizeChanged;
        }

        private void ListViewSizeChanged(object Sender, SizeChangedEventArgs Arguments)
        {
            ListView View = Sender as ListView;
            GridView Grid = (Sender as ListView).View as GridView;

            double ViewWidth = View.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            Grid.Columns[0].Width = ViewWidth * 0.50; // Name
            Grid.Columns[1].Width = ViewWidth * 0.25; // Version
            Grid.Columns[2].Width = ViewWidth * 0.25; // Update
        }

        private void UpdateApplication()
        {
            string ProcessID = Process.GetCurrentProcess().Id.ToString();
            string ExecutableName = Path.GetFileName(AppDomain.CurrentDomain.FriendlyName);
            string ExecutablePath = Directory.GetCurrentDirectory();
            string ChangelogURL = "https://gitlab.com/Togel/wowaddonupdater/raw/master/Binaries/Changelog.xml?inline=false";
            string UpdateURL = "https://gitlab.com/Togel/wowaddonupdater/raw/master/Binaries/WoWAddonUpdater.zip?inline=false";
            string UpdaterURL = "https://gitlab.com/Togel/autoupdater/raw/master/Binaries/Updater.exe?inline=false";
            string Arguments = ProcessID + " \"" + ExecutableName + "\" " + Version + " \"" + ExecutablePath + "\" \"" + ChangelogURL + "\" \"" + UpdateURL + "\"";

            using (WebClient WebClient = new WebClient())
            {
                WebClient.DownloadFile(UpdaterURL, ExecutablePath + @"\temp.exe");
                File.Delete(ExecutablePath + @"\Updater.exe");
                File.Move(ExecutablePath + @"\temp.exe", ExecutablePath + @"\Updater.exe");
            }

            Process.Start("Updater.exe", Arguments);
        }

        private void RefreshAddonList()
        {
            AddonFinder AddonFinder = new AddonFinder();
            Addons.Clear();

            foreach (Addon Addon in AddonFinder.ScanRepository())
            {
                Addon.CurseProjectID = CurseAPI.GetID(Addon.Name);
                if (Addon.CurseProjectID > 0 && !Blacklist.Contains(Addon.CurseProjectID))
                {
                    Addons.Add(Addon);
                }
            }

            AddonList.ItemsSource = Addons;
            AddonList.Items.Refresh();
        }

        private async Task Update(string AddonName)
        {
            AddonUpdater AddonUpdater = new AddonUpdater();
            string DownloadURL = await CurseAPI.GetDownloadLink(AddonName);
            AddonUpdater.UpdateAddon(DownloadURL);
        }

        private async Task UpdateAll()
        {
            AddonUpdater AddonUpdater = new AddonUpdater();

            foreach (Addon Addon in Addons)
            {
                string DownloadURL = await CurseAPI.GetDownloadLink(Addon.Name);
                AddonUpdater.UpdateAddon(DownloadURL);
            }
        }

        private void OnUpdateAll(object Sender, RoutedEventArgs Arguments)
        {
            _ = UpdateAll();
        }

        private async Task Rescan()
        {
            SetButtonsEnabled(false);
            await Task.Run(() => CurseAPI.UpdateDatabase());
            RefreshAddonList();
            SetButtonsEnabled(true);
        }

        private void OnRescan(object Sender, RoutedEventArgs Arguments)
        {
            _ = Rescan();
        }

        private void OnDownload(object Sender, RoutedEventArgs Arguments)
        {
            int Index = AddonList.Items.IndexOf((Sender as Button).DataContext);
            _ = Update(Addons[Index].Name);
        }

        private void OnBlacklist(object Sender, RoutedEventArgs Arguments)
        {
            Blacklist.Add((AddonList.SelectedItem as Addon).CurseProjectID);
            Properties.Settings.Default.Blacklist = Blacklist;
            RefreshAddonList();
        }

        private void OnClearBlacklist(object Sender, RoutedEventArgs Arguments)
        {
            Properties.Settings.Default.Blacklist.Clear();
            RefreshAddonList();
        }

        private void SetButtonsEnabled(bool Enable)
        {
            ButtonUpdateAll.IsEnabled = Enable;
            ButtonRescan.IsEnabled = Enable;
        }

        private void OnSelectRepository(object Sender, RoutedEventArgs Arguments)
        {
            WinForms.FolderBrowserDialog FolderDialog = new WinForms.FolderBrowserDialog();
            FolderDialog.SelectedPath = Properties.Settings.Default.AddonsDirectory;

            if (WinForms.DialogResult.OK == FolderDialog.ShowDialog())
            {
                Properties.Settings.Default.AddonsDirectory = FolderDialog.SelectedPath;
                AddonRepositoryBox.Text = Properties.Settings.Default.AddonsDirectory;
            }
        }

        private void OnExit(object Sender, EventArgs Arguments)
        {
            Properties.Settings.Default.Save();
        }

        private CurseForge CurseAPI = new CurseForge();
        private List<Addon> Addons = new List<Addon>();
        private List<uint> Blacklist = Properties.Settings.Default.Blacklist;
        private readonly string Version = "0.3.0";
    }
}
